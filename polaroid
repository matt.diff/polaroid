#!/usr/bin/env python

import argparse
import json
import requests

valid_keys = [
    'acceptedResourceRoles',
    'args',
    'backoffFactor',
    'backoffSeconds',
    'cmd',
    'constraints',
    'container',
    'cpus',
    'dependencies',
    'deployments',
    'disk',
    'env',
    'executor',
    'fetch',
    'gpus',
    'healthChecks',
    'id',
    'instances',
    'ipAddress',
    'killSelection',
    'labels',
    'maxLaunchDelaySeconds',
    'mem',
    'portDefinitions',
    'ports',
    'readinessChecks',
    'requirePorts',
    'residency',
    'secrets',
    'storeUrls',
    'taskKillGracePeriodSeconds',
    'tasksHealthy',
    'tasksRunning',
    'tasksStaged',
    'tasksUnhealthy',
    'unreachableStrategy',
    'upgradeStrategy',
    'uris',
    'user',
    'version',
    'versionInfo'
]

default_keys = [
    'id',
    'cmd',
    'args',
    'env',
    'instances',
    'cpus',
    'mem',
    'disk',
    'gpus',
    'constraints',
    'fetch',
    'maxLaunchDelaySeconds',
    'healthChecks',
    'container',
    'labels',
    'secrets'
]


def parseArgs():
    """Command-line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-m', '--marathon', type=str,
        help='The full URL to marathon, e.g. "http://host.domain"', required=True)
    parser.add_argument(
        '-p', '--port', type=int,
        help='Marathon listening port', default='8080', required=False)
    parser.add_argument(
        '-d', '--directory', type=str,
        help='Target directory for marathon config file output', required=True)
    parser.add_argument(
        '-i', '--input', type=str,
        help='Comma-separated list of desired keys to parse', default=default_keys, required=False)
    args = parser.parse_args()
    return args

def get_marathon_apps(marathon, port):
    """Returns a (somewhat) validated requests.content JSON object for parsing"""
    url = marathon + ':' + str(port) + '/v2/apps'
    r = requests.get(url)
    if r.status_code != 200:
        raise ValueError('request failed with HTTP code %d' % (r.status_code))
    apps_json = json.loads(r.content)
    # returns type list (of dicts)
    return apps_json['apps']

def validate_keys(keys):
    """Key validation against full Marathon key list"""
    if len(keys) == 0:
        raise KeyError('no keys in key list')
    if not 'id' in keys:
        raise KeyError('\'id\' needs to be in the key list')
    keys_set = set(keys)
    valid_keys_set = set(valid_keys)
    bad_keys = keys_set.difference(valid_keys_set)
    if len(bad_keys) > 0:
        raise ValueError('Invalid keys - %s' % ', '.join(map(str, bad_keys)))

def check_key_dupes(keys):
    """Fails on any key duplicates in input list"""
    for key in valid_keys:
        if keys.count(key) > 1:
            raise ValueError('Multiple occurrences of key %s' % (key))

def write_to_file(app_list, directory):
    """Currently only supports output to flat directory"""
    for app in app_list:
        content = json.dumps(app, indent=2, separators=(',', ': '))
        app_id = app['id']
        file_path = directory + '/' + app_id.replace('/', '.')[1:] + '.json'
        out_file = open(file_path, 'w')
        out_file.write('%s\n' % content)
        out_file.close()

def key_filter(app_list, keys, valid_keys):
    """Scrubs undesired keys from output"""
    prefiltered_apps = app_list
    keys_set = set(keys)
    valid_keys_set = set(valid_keys)
    key_diff = list(valid_keys_set.difference(keys_set))
    trimmed_apps = []
    for app in prefiltered_apps:
        for key in key_diff:
            try:
                del app[key]
            except KeyError:
                pass
        trimmed_apps.append(app)
    # Returns type list (of dicts)
    return trimmed_apps

def zeroize_servicePort(apps):
    """servicePort does not need to be enumerated for app submission, should remain 0"""
    unaltered_apps = apps
    index = 0
    altered_apps = []
    for app in unaltered_apps:
        for index in range(0,65536):
            try:
                app['container']['docker']['portMappings'][index]['servicePort'] = 0
                index += 1
            # Drop out of loop once all list elements are walked
            except IndexError:
                index = 0
                altered_apps.append(app)
                break
            # Some apps have no portMappings list elements
            except KeyError:
                continue
    return altered_apps

def main():
    """Executor"""
    args = parseArgs()
    keys = args.input
    if isinstance(keys, str):
        keys = keys.split(',')
    validate_keys(keys)
    check_key_dupes(keys)
    apps = get_marathon_apps(args.marathon, args.port)
    trimmed_apps = key_filter(apps, keys, valid_keys)
    zeroize_servicePort(trimmed_apps)
    write_to_file(trimmed_apps, args.directory)


if __name__ == '__main__':
    main()
