from setuptools import setup

config = {
    'name':'polaroid',
    'version':'0.0.1',
    'scripts':['polaroid'],
    'install_requires': ['requests']
}

setup(**config)
